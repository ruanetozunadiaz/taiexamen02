import express from "express";
export const router = express.Router();

router.get("/", (req, res) => {
  res.render("index", { titulo: "Examen 02 [Ruanet Ozuna]" });
});

router.get("/pagos", (req, res) => {
  res.render("pagos", { 
    titulo: "Examen 02 [Ruanet Ozuna]",
    txtNumDocente: req.query.txtNumDocente,
    txtNombreDocente: req.query.txtNombreDocente,
    txtDomicilio: req.query.txtDomicilio,
    txtNombreDocente: req.query.txtNombreDocente,
    txtPagoHora: req.query.txtPagoHora,
    txtHorasImpartidas: req.query.txtHorasImpartidas,
    impuesto: "",
    pagoBasePorHora: "",
    pagoTotal: "",
    bono: "",
    totalAPagar: "",
    isPost: false
  });
});

router.post("/pagos", (req, res) => {
    const { txtNumDocente, txtNombreDocente, txtDomicilio, nivel, txtPagoHora, txtHorasImpartidas, hijos } = req.body;
  
    let pagoBasePorHora;
    switch (nivel) {
      case "1":
        pagoBasePorHora = parseFloat(txtPagoHora) * 1.3;
        break;
      case "2":
        pagoBasePorHora = parseFloat(txtPagoHora) * 1.5;
        break;
      case "3":
        pagoBasePorHora = parseFloat(txtPagoHora) * 2;
        break;
      default:
        pagoBasePorHora = parseFloat(txtPagoHora);
    }
  
    const pagoTotal = pagoBasePorHora * parseFloat(txtHorasImpartidas);
    const impuesto = pagoTotal * 0.16;
  
    let bono = 0;
    switch (hijos) {
      case "1-2":
        bono = pagoTotal * 0.05;
        break;
      case "3-5":
        bono = pagoTotal * 0.1;
        break;
      case "6-10":
        bono = pagoTotal * 0.2;
        break;
      default:
        bono = 0;
    }
  
    const totalAPagar = pagoTotal - impuesto + bono;
  
    res.render("pagos", {
      titulo: "Examen 02 [Ruanet Ozuna]",
      pagoTotal: pagoTotal,
      impuesto: impuesto,
      bono: bono,
      totalAPagar: totalAPagar,
      isPost: true,
    });
});

  

export default { router };
